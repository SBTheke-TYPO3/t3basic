<?php
defined('TYPO3_MODE') or die();

(function($extKey) { // Wrap code in function [TASK-be-2]

    // TypoScript Configuration
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($extKey, 'Configuration/TypoScript', 'T3Basic');

    // Prevent default values [BUGFIX-tca-1]
    if(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('bootstrap_package')) {
        unset($GLOBALS['TCA']['tt_content']['columns']['icon_color']['config']['default']);
        unset($GLOBALS['TCA']['tt_content']['columns']['icon_background']['config']['default']);
    }

    // Add FontAwesome icon library [FEATURE-image-2]
    if (TYPO3_MODE === 'BE') {
        $GLOBALS['TBE_STYLES']['skins']['t3basic']['stylesheetDirectories'][] = 'EXT:t3basic/Resources/Public/Backend/Css/';
    }

})('t3basic');
