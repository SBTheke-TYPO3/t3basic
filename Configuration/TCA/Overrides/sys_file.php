<?php
defined('TYPO3_MODE') or die();

(function($table) { // Wrap code in function [TASK-be-2]

    // Show records in list module [TASK-tca-8]
    $GLOBALS['TCA'][$table]['ctrl']['hideTable'] = false;

})('sys_file');
