<?php
defined('TYPO3_MODE') or die();

if(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('bootstrap_package')) {

    (function() { // Wrap code in function [TASK-be-2]

        // Rename options of image sizes [TASK-language-3]
        $GLOBALS['TCA']['tt_content']['columns']['background_image']['config']['overrideChildTca']['columns']['crop']['config']['cropVariants']['default']['title'] = 'LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:option.extralarge';
        $GLOBALS['TCA']['tt_content']['types']['image']['columnsOverrides']['image']['config']['overrideChildTca']['columns']['crop']['config']['cropVariants']['default']['title'] = 'LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:option.extralarge';
        $GLOBALS['TCA']['tt_content']['types']['textpic']['columnsOverrides']['image']['config']['overrideChildTca']['columns']['crop']['config']['cropVariants']['default']['title'] = 'LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:option.extralarge';
        $GLOBALS['TCA']['tt_content']['types']['media']['columnsOverrides']['assets']['config']['overrideChildTca']['columns']['crop']['config']['cropVariants']['default']['title'] = 'LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:option.extralarge';
        $GLOBALS['TCA']['tt_content']['types']['textmedia']['columnsOverrides']['assets']['config']['overrideChildTca']['columns']['crop']['config']['cropVariants']['default']['title'] = 'LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:option.extralarge';
        $GLOBALS['TCA']['tx_bootstrappackage_card_group_item']['types']['1']['columnsOverrides']['image']['config']['overrideChildTca']['columns']['crop']['config']['cropVariants']['default']['title'] = 'LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:option.extralarge';
        $GLOBALS['TCA']['tx_bootstrappackage_accordion_item']['types']['1']['columnsOverrides']['media']['config']['overrideChildTca']['columns']['crop']['config']['cropVariants']['default']['title'] = 'LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:option.extralarge';
        $GLOBALS['TCA']['tx_bootstrappackage_carousel_item']['columns']['background_image']['config']['overrideChildTca']['columns']['crop']['config']['cropVariants']['default']['title'] = 'LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:option.extralarge';
        $GLOBALS['TCA']['tx_bootstrappackage_carousel_item']['columns']['image']['config']['overrideChildTca']['columns']['crop']['config']['cropVariants']['default']['title'] = 'LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:option.extralarge';
        $GLOBALS['TCA']['tx_bootstrappackage_tab_item']['types']['1']['columnsOverrides']['media']['config']['overrideChildTca']['columns']['crop']['config']['cropVariants']['default']['title'] = 'LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:option.extralarge';
        $GLOBALS['TCA']['tx_bootstrappackage_timeline_item']['types']['1']['columnsOverrides']['image']['config']['overrideChildTca']['columns']['crop']['config']['cropVariants']['default']['title'] = 'LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:option.extralarge';
        foreach ([1, 3, 4] as $type) {
            $GLOBALS['TCA']['pages']['types'][$type]['columnsOverrides']['thumbnail']['config']['overrideChildTca']['columns']['crop']['config']['cropVariants']['default']['title'] = 'LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:option.extralarge';
        }

    })();

}
