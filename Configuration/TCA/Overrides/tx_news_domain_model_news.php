<?php
defined('TYPO3_MODE') or die();

if(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('news')) {

    (function($table) { // Wrap code in function [TASK-be-2]

        // Image crop variants [FEATURE-news-2]
        $GLOBALS['TCA'][$table]['types'][0]['columnsOverrides']['fal_media']['config']['overrideChildTca']['columns']['crop']['config']['cropVariants'] = $GLOBALS['TCA']['tt_content']['types']['image']['columnsOverrides']['image']['config']['overrideChildTca']['columns']['crop']['config']['cropVariants'];

        // Remove text "[Translate to xyz]" when translating [TASK-tca-2]
        $GLOBALS['TCA'][$table]['columns']['title']['l10n_mode'] = '';

        // Suggest ordering [TASK-news-1]
        $GLOBALS['TCA'][$table]['columns']['related']['config']['suggestOptions']['default']['orderBy'] = 'datetime DESC';

        // Enable BE search for additional fields [TASK-news-3]
        $GLOBALS['TCA'][$table]['ctrl']['searchFields'] = 'uid,title,teaser,bodytext,author,keywords,description,alternative_title,notes';

    })('tx_news_domain_model_news');

}
