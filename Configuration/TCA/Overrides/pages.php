<?php
defined('TYPO3_MODE') or die();

(function($table) { // Wrap code in function [TASK-be-2]

    // Enable meta tag "description" for page type "shortcut" (to pass it to subpages) [TASK-tca-4]
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes($table, '--div--;LLL:EXT:seo/Resources/Private/Language/locallang_tca.xlf:pages.tabs.seo,description', '4', 'after:shortcut');


    // EXAMPLE: Move tab
    /*
     * if(TYPO3_MODE === 'BE') {
     * preg_match('/--div--;LLL:EXT:frontend\/Resources\/Private\/Language\/locallang_tca.xlf:pages.tabs.metadata(.*?--div--|.*$)/s', $GLOBALS['TCA'][$table]['types']['1']['showitem'], $matches);
     * if($matches[0]) {
     * $GLOBALS['TCA'][$table]['types']['1']['showitem'] = preg_replace('/--div--;LLL:EXT:frontend\/Resources\/Private\/Language\/locallang_tca.xlf:pages.tabs.metadata(.*?--div--|.*$)/s', '--div--', $GLOBALS['TCA'][$table]['types']['1']['showitem']);
     * $GLOBALS['TCA'][$table]['types']['1']['showitem'] .= ',' . rtrim($matches[0], '--div--');
     * }
     * }*/

})('pages');
