<?php
defined('TYPO3_MODE') or die();

// Carousel with flexible height [FEATURE-contentelement-2]
if(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('bootstrap_package')) {

    (function ($table) { // Wrap code in function [TASK-be-2]

        /***************
         * Add Content Element
         */
        if (!is_array($GLOBALS['TCA'][$table]['types']['carousel_flexible'])) {
            $GLOBALS['TCA'][$table]['types']['carousel_flexible'] = [];
        }

        /***************
         * Add content element PageTSConfig
         */
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
            't3basic',
            'Configuration/TsConfig/Page/ContentElement/Element/CarouselFlexible.tsconfig',
            'Bootstrap Package Content Element: Carousel Flexible'
        );

        /***************
         * Add content element to selector list
         */
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
            $table,
            'CType',
            [
                'LLL:EXT:t3basic/Resources/Private/Language/backend.xlf:content_element.carousel_flexible',
                'carousel_flexible',
                'content-bootstrappackage-carousel'
            ],
            'carousel_fullscreen',
            'after'
        );

        /***************
         * Assign Icon
         */
        $GLOBALS['TCA'][$table]['ctrl']['typeicon_classes']['carousel_flexible'] = 'content-bootstrappackage-carousel';

        /***************
         * Configure element type
         */
        $GLOBALS['TCA'][$table]['types']['carousel_flexible'] = array_replace_recursive(
            $GLOBALS['TCA'][$table]['types']['carousel_flexible'],
            [
                'showitem' => '
                    --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
                        --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
                        --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.headers;headers,
                        tx_bootstrappackage_carousel_item,
                    --div--;LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:carousel.options,
                        pi_flexform;LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:advanced,
                    --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
                        --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,
                        --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,
                    --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
                        --palette--;;language,
                    --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                        --palette--;;hidden,
                        --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
                    --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
                        categories,
                    --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,
                        rowDescription,
                    --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,
                '
            ]
        );

        /***************
         * Add flexForms for content element configuration
         */
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
            '*',
            'FILE:EXT:bootstrap_package/Configuration/FlexForms/Carousel.xml',
            'carousel_flexible'
        );

    })('tt_content');

}
