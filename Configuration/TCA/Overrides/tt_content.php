<?php
defined('TYPO3_MODE') or die();

(function($table) { // Wrap code in function [TASK-be-2]

    // Prevent that field "frame_class" of content element type "reference" gets value "default" by default [TASK-tca-7]
    $GLOBALS['TCA'][$table]['columns']['frame_class']['config']['default'] = '';


    // Enable multiple selection for field "frame_class" [TASK-tca-5]
    $GLOBALS['TCA'][$table]['columns']['frame_class']['config']['renderType'] = 'selectMultipleSideBySide';
    unset($GLOBALS['TCA'][$table]['columns']['frame_class']['onChange']);


    // Prevent default values like "none" [TASK-template-3]
    $GLOBALS['TCA'][$table]['columns']['background_color_class']['config']['items'][0][1] = '';


    // Improve english and german language labels [TASK-ui-4]
    $GLOBALS['TCA'][$table]['columns']['space_before_class']['config']['items'][0][0] = 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:space_class_none';
    $GLOBALS['TCA'][$table]['columns']['space_after_class']['config']['items'][0][0] = 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:space_class_none';


    // EXAMPLE: Add new headings (don't add them via Page TSconfig because of EXT:cefooter)
    #$GLOBALS['TCA'][$table]['columns']['header_layout']['config']['items'][10] = [0 => 'LLL:EXT:t3basic/Resources/Private/Language/backend.xlf:tt_content.header_layout.example', 1 => 10];


    // Add FontAwesome icon library [FEATURE-image-2]
    $GLOBALS['TCA'][$table]['columns']['icon_set']['config']['items'][] = ['FontAwesome Regular', 'EXT:t3basic/Resources/Public/Icons/FontAwesome/regular/'];
    $GLOBALS['TCA'][$table]['columns']['icon_set']['config']['items'][] = ['FontAwesome Solid', 'EXT:t3basic/Resources/Public/Icons/FontAwesome/solid/'];
    $GLOBALS['TCA'][$table]['columns']['icon_set']['config']['items'][] = ['FontAwesome Brands', 'EXT:t3basic/Resources/Public/Icons/FontAwesome/brands/'];


    // EXT:cefooter: Fix language labels for coherent texts [TASK-language-4]
    $GLOBALS['TCA'][$table]['columns']['subheader']['label'] = 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:subheader_formlabel';
    $GLOBALS['TCA'][$table]['columns']['header_layout']['label'] = 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_layout_formlabel';
    $GLOBALS['TCA'][$table]['columns']['header_link']['label'] = 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_link_formlabel';

})('tt_content');
