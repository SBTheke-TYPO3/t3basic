<?php
defined('TYPO3_MODE') or die();

if(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('bootstrap_package')) {

    (function($table) { // Wrap code in function [TASK-be-2]

        // Show records in list module and in search results in BE [TASK-tca-8]
        $GLOBALS['TCA'][$table]['ctrl']['hideTable'] = 0;
        $GLOBALS['TCA'][$table]['ctrl']['searchFields'] = 'header,subheader,nav_title,bodytext,link';


        // Heading for slider items not required [TASK-tca-9]
        $GLOBALS['TCA'][$table]['columns']['header']['config']['eval'] = 'trim';

    })('tx_bootstrappackage_carousel_item');

}
