/***************************************************************
* EXAMPLE: Remove content elements
*/
#TCEFORM.tt_content.CType.removeItems := addToList(textteaser, texticon, icon_group, listgroup)


/***************************************************************
* Remove headlines [TASK-ui-1]
* Hint: Add new headlines in TCA/Overrides/tt_content.php
*/
TCEFORM.tt_content.header_layout.removeItems := addToList(4, 5)


/***************************************************************
* Add and remove some frames
*/
TCEFORM.tt_content.frame_class {
    // Remove options [TASK-tca-6]
    removeItems := addToList(none, default)
    addItems {
        // New frame "Full width" (for breaking out of container) [FEATURE-contentelement-1]
        full-width = LLL:EXT:t3basic/Resources/Private/Language/backend.xlf:tt_content.frame_class.full-width
    }
    types {
        image.addItems {
            // Slider with content element "Images Only" and slick [FEATURE-slider-3]
            slider = LLL:EXT:t3basic/Resources/Private/Language/backend.xlf:tt_content.frame_class.slider
            // Simple images (not responsive) [FEATURE-image-3]
            image-simple = LLL:EXT:t3basic/Resources/Private/Language/backend.xlf:tt_content.frame_class.image-simple
        }
    }
}


/***************************************************************
* Remove unneeded fields [TASK-ui-1]
*/
TCEFORM {
    pages {
        layout.disabled = 1
        newUntil.disabled = 1
        keywords.disabled = 1
        author.disabled = 1
        author_email.disabled = 1
        lastUpdated.disabled = 1
        content_from_pid.disabled = 1
        thumbnail.disabled = 1
        cache_tags.disabled = 1
        fe_login_mode.disabled = 1
        editlock.disabled = 1
        categories.disabled = 1
        canonical_link.disabled = 1
        og_title.disabled = 1
        og_description.disabled = 1
        og_image.disabled = 1
        twitter_title.disabled = 1
        twitter_description.disabled = 1
        twitter_image.disabled = 1
        tx_csseo_tw_creator.disabled = 1
        tx_csseo_tw_site.disabled = 1
    }
    tt_content {
        linkToTop.disabled = 1
        #layout.disabled = 1 # do not disable, it is used for content element "File Links"
        header_position.disabled = 1
        date.disabled = 1
        subheader.disabled = 1
        editlock.disabled = 1
        categories.disabled = 1
        // Remove flexform fields
        pi_flexform.default.customClasses.col21class.disabled = 1
        pi_flexform.default.customClasses.col22class.disabled = 1
        pi_flexform.default.rowClasses.rowCustom.disabled = 1
    }
    tx_news_domain_model_news {
        keywords.disabled = 1
        notes.disabled = 1
    }
    tx_news_domain_model_tag {
        seo_text.disabled = 1
        notes.disabled = 1
    }
}


/***************************************************************
* Remove some fields depending on extensions [TASK-ui-2]
*/
[extension.isLoaded('indexed_search')]
[ELSE]
    TCEFORM.pages.no_search.disabled = 1
[END]

[extension.isLoaded('felogin')]
[ELSE]
    TCEFORM.pages.fe_login_mode.disabled = 1
[END]


/***************************************************************
* Remove SEO fields on pages where SEO fields are provided by plugin (e.g. on news detail page) [TASK-seo-4]
*/
[108 in tree.rootLineIds]
    TCEFORM {
        pages {
            seo_title.disabled = 1
            description.disabled = 1
            tx_csseo_keyword.disabled = 1
        }
    }
[END]
