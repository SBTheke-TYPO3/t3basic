#2020-09-22  Sven Burkert  <bedienung@sbtheke.de>

* Version 8.0.0
* Update to TYPO3 10.4
* Development from scratch
* htaccess
    * Redirect to https [TASK-htaccess-1]
    * Redirect to www [TASK-htaccess-2]
    * Redirect url with trailing slash [TASK-url-1]
    * "keep-alive" for faster page speed [TASK-htaccess-4]
* Configuration of GIT [TASK-git-1]
* General configuration (LocalConfiguration and AdditionalConfiguration) [TASK-config-1]
    * Reset to default TYPO3 logo for backend logo [TASK-be-3]
    * Set company logo as login logo [TASK-be-5]
    * Compression for images [TASK-image-2]
    * Debugging only in development context [TASK-debug-1]
    * Enable interlaced images (https://forge.typo3.org/projects/typo3cms-core/repository/revisions/f7b516d0ab6dccbb89e3eafcfb9e937d0a1877a3) [TASK-interlace-1]
    * And more
* Site Configuration / Routing
    * Speaking URLs [FEATURE-url-1]
    * URLs with trailing slash [TASK-url-1]
    * Configuration of 404 error handling (Site Configuration -> Error Handling) [FEATURE-url-2]
    * Configuration of robots.txt and sitemap.xml [FEATURE-url-3]
* Configuration of caching
    * Caching of frontend [TASK-cache-2]
    * Caching: Disable all TYPO3 caches in development environment [TASK-cache-1]
* Configuration and optimization of BE [TASK-be-1]
    * Wrap code in ext_localconf.php, ext_tables.php and Configuration/TCA/Overrides/ in a function because of performance and to ensure new variables are only available in current scope [TASK-be-2]
    * Prevent "copy 1" when copying pages, content elements and records [TASK-be-4]
    * Backend user and group configuration [TASK-beuser-1]
        * Rights management according to https://typo3worx.eu/2017/02/typo3-backend-user-management/ [TASK-beuser-2]
        * Allow flushing of TYPO3 cache for editor [TASK-beuser-3]
    * Clean, tidy and structured user interface for admins and editors
        * Remove unneeded fields and options [TASK-ui-1]
        * Remove fields when corresponding extension isn't installed [TASK-ui-2]
        * Set default values [TASK-ui-3]
        * Improve english and german language labels [TASK-ui-4]
        * Optimize "New Content Element Wizard"
            * Order of tabs [TASK-ui-wizard-1]
            * Rename tabs (so they fit in one row) [TASK-ui-wizard-2]
            * Rename content elements and change descriptions [TASK-ui-wizard-3]
            * Remove irrelevant and duplicate content elements [TASK-ui-wizard-4]
            * Rearrange content elements between tabs [TASK-ui-wizard-5]
            * Rearrange content elements in tabs [TASK-ui-wizard-6]
    * Multidomain configuration (root2.basic.local) [TASK-config-2]
    * Optimize TCA [TASK-tca-1]
        * Ensure that TCA overrides (Configuration/TCA/Overrides/) only apply if extension is loaded [TASK-tca-3]
        * Enable meta tag "description" for page type "shortcut" (to pass it to subpages) [TASK-tca-4]
        * Remove text "Translate to xyz" when translating pages, content elements and records [TASK-tca-2], because of two reasons:
            * It's annoying to remove the prefixed text every time.
            * Speaking URL contains "translate-to"
        * Changes for field "frame_class"
            * Enable multiple selection [TASK-tca-5]
            * Remove options "Default" and "None" [TASK-tca-6]
            * Prevent that of content element type "reference" gets value "default" by default [TASK-tca-7]
            * New frame "Full width" (for breaking out of container) [FEATURE-contentelement-1]
            * New frame "Simple image" (for non responsive images) [FEATURE-image-3]
* Configuration and optimization of FE [TASK-fe-1]
    * Move favicon in root directory (for non-HTML pages like PDFs or images opened directly in browser) [TASK-fe-6]
    * JavaScript default file [TASK-javascript-4]
    * CSS
        * Usage of Bootstrap 4 (https://getbootstrap.com) [FEATURE-css-1]
        * Optimize buttons: Spacing to buttons in new line [TASK-css-11]
        * Optimize pagination: Hide "prev" and "next" on mobile devices to enable more items on smaller displays [TASK-css-3]
        * Automatic hyphenation for words [TASK-css-4]
        * Ensure that debug output is visible (and not overlayed by fixed header) [TASK-css-5]
        * Main navigation: Align dropdown centrally [TASK-css-9]
    * Optimize and fix print layout
        * Clean print view [TASK-print-1]
        * Hide many elements (navigations, forms, breadcrumb, ...) [TASK-print-2]
        * Replace some elements with text (e.g. carousel, video) [TASK-print-3]
    * Optimize template of system mails (logo, translation, imprint in footer) [TASK-mail-1]
* Enhancement of FE
    * Top and footer meta navigation, changeable by editor [FEATURE-nav-1]
    * Enable icons in top meta navigation [FEATURE-nav-2]
    * Implement slider with content element "Images Only" and slick [FEATURE-slider-3]
    * Optimizations for JavaScript [TASK-fe-5]
    * Spam protection of email addresses with JavaScript [TASK-fe-2]
    * Browser color for mobile devices [TASK-fe-3]
    * Support of Retina images [TASK-fe-4]
* System EXT:impexp
    * Provide preset to export content with images (see table "tx_impexp_presets") [TASK-DB-2]
* System EXT:extbase
    * TypoScript configuration [TASK-typoscript-4]
    * Bootstrap 4 HTML for Paginate ViewHelper [TASK-template-5]
* System EXT:fluid
    * Adapt language labels [TASK-language-8]
* System EXT:rte_ckeditor
    * Optimize RTE configuration [TASK-rte-1]
        * Remove unneeded block formats [TASK-rte-4]
        * Remove unneeded buttons [TASK-rte-2]
        * Remove target selector (target is set automatically: "_blank" for all external links) [TASK-rte-3]
    * Provide custom stylesheet and overwrite colors [TASK-rte-5]
* System EXT:form [FEATURE-form-1]
    * Configuration [TASK-form-2]
    * Store forms in /user_upload/forms/ (instead of /user_upload/) [TASK-form-6]
    * Store uploaded files in forms in /user_upload/forms/uploaded_files/ (instead of /user_upload/) [TASK-form-3]
    * Prevent access to folder with uploaded files [TASK-form-7]
    * Provide contact and example form [TASK-form-5]
    * Remove form of EXT:bootstrap_package in BE module [TASK-form-4]
    * Implement simple solution for links in form (e.g. privacy or data protection), see contact form [TASK-form-8]
* System EXT:indexed_search for search [FEATURE-search-1]
    * Provide layout template and wrap plugin in class [TASK-search-4]
    * Adapt templates for Bootstrap [TASK-search-5]
    * Optimize search input field (type=search) [TASK-search-3]
    * Enhancement of EXT:indexed_search PageBrowsing ViewHelper class for better HTML (CSS classes, jQuery) and compatibility to Bootstrap and ARIA support [TASK-search-2]
    * Searchbox in header [FEATURE-search-2]
    * Fix translation issue (rules.text) [BUGFIX-search-1]
    * Improved and reasonable language labels [TASK-language-5]
    * Configuration of speaking URLs [TASK-search-7]
* System EXT:felogin [FEATURE-login-1]
    * Configuration [TASK-login-1]
    * Adapt templates for Bootstrap [TASK-login-2]
    * Adapt email template of password recovery [TASK-mail-2]
    * Distinguish messages (e.g. login error) with default Bootstrap styles [TASK-css-6]
    * Improved and reasonable language labels [TASK-language-6]
* EXT:bootstrap_package: Configure, optimize, enhance and bugfix
    * Submit many bugfixes for EXT:bootstrap_package: https://github.com/benjaminkott/bootstrap_package/issues/created_by/SventB
    * Compare and optimize htaccess
    * Optimize TCA
        * Special contents (accordion, carousel, tabs, ...) can be found in BE search now (https://github.com/benjaminkott/bootstrap_package/issues/727) [TASK-tca-8]
        * Heading for slider items not required [TASK-tca-9]
        * Prevent default values for field "icon_color" and "icon_background" in table "tt_content" [BUGFIX-tca-1]
    * Backend Layouts
        * Remove not needed BE layouts [TASK-belayout-1]
        * Rename BE layouts [TASK-belayout-2]
        * Enable content below sub navigation [FEATURE-belayout-3]
        * Remove additional columns (e.g. footer columns) [TASK-belayout-4]
        * Modify icons (e.g. without footer columns) [TASK-belayout-5]
        * Activate slide for border columns [TASK-belayout-6]
    * Improved and reasonable language labels (mainly german translations) [TASK-language-2]
        * Rename options of image sizes [TASK-language-3]
    * Optimize indexing of pages: exclude submenu (https://github.com/benjaminkott/bootstrap_package/issues/461) [BUGFIX-search-1]
    * Optimize templates [TASK-template-7]
        * Logical structure (introduce Partials/Page/Structure/Header and Partials/Page/Structure/Footer) [TASK-template-1]
        * Adapt template "None" (can be used for popups) [TASK-template-8]
        * Adapt templates of EXT:form for Bootstrap [TASK-form-9]
        * Add class and icon for "link to top" [TASK-template-6]
        * Automatic headers: First header is H1, all other h2, headers not in main columns are h3 [FEATURE-headings-1]
        * Fix breadcrumb (https://github.com/benjaminkott/bootstrap_package/issues/690) [BUGFIX-template-2]
        * Set correct classes for Bootstrap 4 (https://github.com/benjaminkott/bootstrap_package/issues/726) [BUGFIX-template-1]
        * Use class "list-inline" for navigations [TASK-nav-1]
        * Disable lazy loading of images because of problems with interactive elements or because it's unneeded [TASK-image-1]
        * Remove unneeded classes like "frame-layout-0", "frame-space-before-none", "frame-space-after-none", "frame-background-none", "frame-no-backgroundimage" [TASK-template-3]
        * Add classes for plugin type, e.g. "frame-type-list-news_pi1" [TASK-template-4]
        * Valid HTML code
            * Add tag "main", remove attribute "role" (https://github.com/benjaminkott/bootstrap_package/issues/462) [TASK-template-2]
            * Replace section-tag with div-tag (sections need a heading) [BUGFIX-template-4]
            * Remove attribute "navigation" from tag "nav" [BUGFIX-template-5]
            * Remove deprecated attribute "intrinsicsize" for images (https://github.com/benjaminkott/bootstrap_package/issues/963) [BUGFIX-template-3]
    * Optimize content elements
        * Make TypoScript constants from "page.theme" available [TASK-contentelement-2]
        * Optimize content element "Uploads": Add "download" attribute [TASK-contentelement-1]
        * Optimize content element "Sitemap": Do not show root page and fix TypoScript (https://github.com/benjaminkott/bootstrap_package/issues/955) [BUGFIX-contentelement-1]
        * Optimize content element "Accordion": Provide setting "accordionShowFirst" to show/hide first element of accordion [TASK-accordion-2]
        * Carousel with flexible height [FEATURE-contentelement-2]
    * Optimize CSS
        * Own theme (written in SCSS) with possibility to exclude parts (for smaller CSS files) [TASK-css-1]
        * Padding for content elements with background image or color [TASK-css-2]
        * Sticky header, optionally with transition [FEATURE-css-2]
        * Show sub navigation on mobile devices [TASK-css-7]
        * Remove variables defined as TypoScript constant from _variables.scss [TASK-css-8]
        * Add often used variables [TASK-css-10]
    * Optimize JavaScript
        * Remove unneeded JavaScript files (modernizr) [TASK-javascript-1]
        * Include latest jQuery library from Google CDN (has also the advantage, that jQuery version can be easily changed) [TASK-javascript-2]
        * Improved smooth scroll (top padding; correct top padding when opening URL with anchor directly, provide "#scrollHere" for scrolling to target element when opening a page) [TASK-javascript-3]
    * Adapt TypoScript configuration [TASK-typoscript-1]
        * Configuration option for "permanent link in footer to scroll to top" [FEATURE-footer-1]
        * Language configuration [TASK-typoscript-2]
        * Configuration of breadcrumb [TASK-typoscript-3]
        * Provide condition for checking content elements on current page: [t3basic.isElementOnCurrentPageInCurrentLanguage('image')] [FEATURE-typoscript-1]
        * Open file links in new tab [TASK-typoscript-5]
    * Provide more content examples (pages "Text and Background Image", "Spaces", "Link to top") [TASK-db-1]
    * Provide content example for responsive iframe [TASK-db-3]
    * Add current year in footer [FEATURE-year-1]
    * Disable WebFontLoader and caching of webfonts [TASK-fonts-1]
    * Add FontAwesome icon library [FEATURE-image-2]
    * Lightbox for images [FEATURE-lightbox-1]
        * Icon for images with lightbox [FEATURE-lightbox-3]
        * Transparent background in lightbox [TASK-lightbox-1]
        * Show title and description in lightbox only once, if they are identical [FEATURE-lightbox-2]
    * EXT:indexed_search: Displaying of rules with "collapse" (instead of "popover") [TASK-search-6]
* EXT:gridelements for grids [FEATURE-grid-1]
    * Use default layout template which is used for content elements also for grids of EXT:gridelements (EXT:t3basic/Resources/Private/Layouts/ContentElements/Default.html) [TASK-gridelements-1]
    * Implement fully configurable slick slider (http://kenwheeler.github.io/slick/) [FEATURE-slider-1]
    * Implement Bootstrap slider (https://getbootstrap.com/docs/3.3/javascript/#carousel) [FEATURE-slider-2]
    * Bootstrap slider without fixed height (use class "carousel-flexible") [TASK-slider-1]
    * Include JS library for slider only, if slider exists on page (asset collector) [TASK-slider-2]
    * Sorting of grid elements [TASK-gridelements-2]
    * Fix issue with plugins with FlexForm (https://gitlab.com/coderscare/gridelements/-/issues/150) [BUGFIX-gridelements-1]
* EXT:bootstrap_grids for predefined grids [FEATURE-grid-2]
    * Add new grid with 1 column (https://github.com/laxap/bootstrap_grids/issues/10) [FEATURE-grid-3]
    * Fix translation, improve field description for editor [TASK-bootstrapgrids-2]
    * Enable hiding on "extra small" devices [FEATURE-grid-4]
    * Adapt accordion for bootstrap [TASK-accordion-1]
    * Provide setting "accordionShowFirst" to show/hide first element of accordion [TASK-accordion-2]
    * Remove unneeded grid elements [TASK-bootstrapgrids-1]
* SEO
    * Speaking URLs [FEATURE-url-1]
    * Set robots meta tag to "noindex,nofollow" for Development context [TASK-seo-3]
    * Inherit meta tag "description" to subpages [TASK-seo-2]
    * Browser title configuration [TASK-seo-6]
    * System EXT:seo [FEATURE-seo-1]
        * XML sitemap for every language, see "/?type=1533906435" [TASK-seo-1]
        * XML sitemap for EXT:news, see "/?type=1533906435" [TASK-news-6]
    * EXT:cs_seo for search engine optimizations [FEATURE-seo-2]
        * Preview of Google search result in page properties
        * Analysis of page, optional for a focus keyword
        * Analysis of records of EXT:news [TASK-seo-5]
        * Remove SEO fields on pages where SEO fields are provided by plugin (e.g. on news detail page) [TASK-seo-4]
        * Optional tracking with Google Analytics, Google Tag Manager or Matomo [TASK-tracking-1]
        * Disable tracking for Development and Local context [TASK-tracking-2]
        * Disable tracking if no consent was given [TASK-tracking-4]
        * Opt-Out link for Google Analytics (http://www.basic104.local/datenschutz/) [TASK-tracking-3]
        * Bugfix of breadcrumb structured data (https://github.com/clickstorm/cs_seo/issues/276) [BUGFIX-seo-1]
        * Use logo when sharing a page [FEATURE-seo-3]
    * And more
* EXT:news [FEATURE-news-1]
    * Optimize TCA [TASK-news-1]
        * Enable BE search not only for uid and title, but more, like teaser and bodytext [TASK-news-3]
    * TypoScript configuration [TASK-news-2]
    * Improved and reasonable language labels [TASK-language-7]
    * Optimize styles [TASK-news-4]
    * Optimize templates [TASK-newstemplate-1]
        * Optimize template for category menu [TASK-newstemplate-2]
        * Optimize template for date menu: Use accordion [FEATURE-newstemplate-1]
        * Optimize template for news list: Adapt template for Bootstrap [TASK-newstemplate-3]
        * Optimize template for news detail page [TASK-newstemplate-4]
            * Browser title contains the page title as prefix (e.g. "News: Example News") [TASK-newstemplate-5]
            * Optimize HTML structure for images and videos [TASK-newstemplate-6]
        * Optimize list of categories and tags (comma separated) [TASK-newstemplate-7]
        * Enable different crop variants for images (not yet for [media] placeholder, see https://github.com/georgringer/news/issues/931) [FEATURE-news-2]
        * Attribute "alt" for dummy images [BUGFIX-news-1]
        * Enable different layouts for news list
            * Slider [FEATURE-news-4]
            * 2 and 3 columns with Bootstrap Cards (https://getbootstrap.com/docs/4.3/components/card/) [FEATURE-news-5]
        * Bootstrap 4 HTML for Paginate ViewHelper [TASK-newstemplate-8]
        * Optimize URLs in Paginate ViewHelper [TASK-newstemplate-9]
    * Enhance breadcrumb path with news title [FEATURE-news-3]
    * Linkhandler to link on news detail pages with RTE [FEATURE-news-6]
    * Preview of (hidden) news from BE (only for logged in BE users) [FEATURE-news-7]
    * SEO
        * Configuration of speaking URLs [TASK-news-5]
        * Configuration of XML sitemap, see "/?type=1426844832" [TASK-news-6]
        * Remove meta tags on news detail page, EXT:news provides them [TASK-news-7]
    * RSS feed, see "/feed.rss" [FEATURE-newsrss-1]
        * Feed can be enabled with TS constant [TASK-newsrss-1]
        * Configuration of plugin with feed settings: Editor can configure feed by editing plugin content element [FEATURE-newsrss-2]
        * Channel image (TS constant plugin.tx_news.rss.channel.image) [TASK-newsrss-2]
        * Meta tag to notify search bots or feed readers [FEATURE-newsrss-3]
* EXT:default_upload_folder: Configure upload path of files which are uploaded via "Select & upload images", e.g. for content elements, pages or news [FEATURE-fileupload-1]
* EXT:cefooter for showing informations of a content element in the footer of the content element in TYPO3 BE in the page module [FEATURE-be-1]
    * Fix language labels for coherent texts [TASK-language-4]
* EXT:image_autoresize to automatically resize down huge images on upload [FEATURE-image-1]
* EXT:crawler for indexing contents and records for EXT:indexed_search [FEATURE-search-3]

#2019-05-14  Sven Burkert  <bedienung@sbtheke.de>

* Version 7.0.0
* Update to TYPO3 9.5
* Development from scratch
* General configuration (LocalConfiguration and AdditionalConfiguration)
    * Compression for images
    * Debug messages only in development context
    * Enable interlaced images (https://forge.typo3.org/projects/typo3cms-core/repository/revisions/f7b516d0ab6dccbb89e3eafcfb9e937d0a1877a3)
    * And more
* EXT:bootstrap_package: Configure, optimize, enhance and bugfix
    * Submit many bugfixes for EXT:bootstrap_package: https://github.com/benjaminkott/bootstrap_package/issues/created_by/SventB
    * Compare and optimize htaccess
    * Optimize TCA: special contents (accordion, carousel, tabs, timeline) can be found in BE search (https://github.com/benjaminkott/bootstrap_package/issues/727)
    * Backend Layouts: Remove not needed layouts
    * Backend Layouts: Remove additional columns (e.g. footer columns)
    * Backend Layouts: Modified icons (e.g. without footer columns)
    * Backend Layouts: Enable slide for border columns
    * Backend Layouts: Modify backend layout "Simple" (for popups, lightbox content, ajax or other)
    * Improved and reasonable language configuration
    * Optimize indexing of pages: exclude submenu (https://github.com/benjaminkott/bootstrap_package/issues/461)
    * Remove unneeded JavaScript files
    * Include latest jQuery library from Google CDN (has also the advantage, that jQuery version can be easily changed)
    * Remove unneeded classes like "frame-layout-0", "frame-space-before-none", "frame-space-after-none", "frame-background-none", "frame-no-backgroundimage"
    * Optimize templates
        * Optimize Fluid templates: Logical structure (introduce Partials/Page/Structure/Header)
        * Optimize page templates: Add tag "main", remove attribute "role" (https://github.com/benjaminkott/bootstrap_package/issues/462)
        * Optimize content templates
        * Remove f:spaceless (because whitespace between buttons were removed)
    * Optimize RTE configuration
        * Remove unneeded buttons and styles
        * Add missing styles (https://github.com/benjaminkott/bootstrap_package/issues/728)
    * Prevent default values for field "icon_color" and "icon_background" in table "tt_content"
    * Support of preview images from PDF
    * Provide more content examples
    * Add current year in footer
    * Own theme (SCSS/CSS) with possibility to exclude parts for better performance
    * Disable WebFontLoader and caching of webfonts (https://github.com/benjaminkott/bootstrap_package/issues/751)
    * Sticky header, optionally with transition
    * Prepare slider without fixed height (use class "carousel-noFixedHeight")
    * Configuration option for "permanent link in footer to scroll to top"
    * Improved smooth scroll (top padding; correct top padding when opening url with anchor directly)
    * EXT:indexed_search: Displaying of rules with "collapse" (instead of "popover")
    * Lightbox for images
        * Icon for images with lightbox
        * Transparent background in lightbox
        * Show title and description in lightbox only once, if they are identical
* htaccess
    * Redirect to https
    * Redirect to www
    * Redirect url with trailing slash
    * "keep-alive" for faster page speed
* Extbase
    * Bootstrap 4 HTML for Paginate ViewHelper
* CSS
    * Usage of Bootstrap 4 (https://getbootstrap.com)
    * Optimize pagination: Hide "prev" and "next" on mobile devices to enable more items on smaller displays
    * Automatic hyphenation for words
    * Prepare "floating labels" for form fields (https://getbootstrap.com/docs/4.3/examples/floating-labels/)
* Reset default images for login logo and backend logo
* Move favicon in root directory (for non-HTML pages like PDFs or images opened directly in browser)
* Site Configuration
    * Speaking urls
    * Urls contain a trailing slash
    * Configuration of 404 error handling (Site Configuration -> Error Handling)
    * Configuration of robots.txt and sitemap.xml
* Remove text "[Translate to xyz]" when translating pages, content elements and records, because of two reasons:
    * It's annoying to remove the prefixed text every time.
    * Speaking URL contains "translate-to"
* Prepare many configurations
* Pre-configuration of second website (root2.basic.local)
* Add new backend layout "Default, Full width", mainly as example for new backend layouts
* Enable links between webroots in same TYPO3 backend
* Backend user management according to https://typo3worx.eu/2017/02/typo3-backend-user-management/
* TypoScript: Provide condition for checking content elements on current page: [SBTheke\T3basic\Condition\IsElementOnCurrentPageCondition = accordion]
* Optimize and fix print layout
    * Clean print view
    * Hide many elements (navigations, forms, breadcrumb, ...)
    * Replace some elements with text (carousel, video)
* Wrap contents in ext_localconf.php, ext_tables.php and Configuration/TCA/Overrides/ in a function because of performance and to ensure new variables are only available in current scope
* Ensure that TCA overrides (Configuration/TCA/Overrides/) only apply if extension is loaded
* Optimize Backend: Clean, tidy and structured user interface for editor
    * Remove unneeded fields
    * Remove fields when corresponding extension isn't installed
    * Set default values
    * Fix german translations (e.g. for EXT:news)
    * Optimize "New Content Element Wizard"
        * Order of tabs
        * Rename tabs (so they fit in one row)
        * Rename content elements (e.g. "Text" instead of "Regular Text Element")
        * Change description of content elements
        * Translation to german (e.g. for "Form")
* Enable meta tag "description" for page type "shortcut" (to pass it to subpages)
* Top and footer meta navigation
* Configuration of caching
    * Caching of frontend
    * Options for clear cache in BE for editors
    * Disable cache in development environment
* Changes for field "frame_class"
    * Enable multiple selection for field "frame_class"
    * Remove options "Default" and "None"
* Development
    * Ensure that debug output is visible (and not overlayed by fixed header)
* SEO
    * XML sitemap for every language and for EXT:news
    * and more (speaking urls, ...)
* EXT:cs_seo for search engine optimizations
    * Preview of Google search result in page properties
    * Analysis of page, optional for a focus keyword
    * Optional tracking with Google Analytics or Piwik
    * Disable tracking for Development context
    * Opt-Out link for Google Analytics (http://www.basic95.local/datenschutz)
    * Remove SEO fields on pages where SEO fields are provided by plugin (e.g. on news detail page)
    * Set robots meta tag to "noindex,nofollow" for Development context
    * Enable different browser title prefix for different languages
* EXT:default_upload_folder: Configure upload path of files which are uploaded via "Select & upload images", e.g. for content elements, pages or news
* EXT:form
    * Store forms in /user_upload/forms/ (instead of /user_upload/)
    * Store uploaded files in forms in /user_upload/forms/uploaded_files/ (instead of /user_upload/)
    * Provide example form
* EXT:rte_ckeditor
    * Remove unneeded buttons
    * Remove target selector (target is set automatically: "_blank" for all external links)
* EXT:felogin: Configuration
* EXT:indexed_search for search
    * Enhancement of indexed_search ViewHelper class for better HTML (CSS classes, jQuery) and compatibility to Bootstrap and ARIA support
    * Optimize search input field (type=search)
    * Search field in header
* EXT:cefooter for showing informations of a content element in the footer of the content element in TYPO3 BE in the page module
* EXT:news
    * Preconfiguration, optimized templates, styles, RSS feed, browser title, speaking url, breadcrumb path and meta data on detail page
    * Enable different layouts for LIST (2 columns, 3 columns, slider)
        * 2 and 3 columns with Bootstrap Cards (https://getbootstrap.com/docs/4.3/components/card/)
    * Enable different crop variants for images (not for [media] placeholder, see https://github.com/georgringer/news/issues/931)
    * Linkhandler to link on news detail pages with RTE
    * Optimize HTML structure for videos in news details
    * Optimize template for category menu
    * Preview of (hidden) news from BE (only for logged in BE users)
    * Enable BE search not only for uid and title, but more, like teaser and bodytext
    * Browser title contains "News:" (the page title is used as prefix)
    * Attribute "alt" for dummy images
    * RSS feed
        * Feed can be enabled with TS constant
        * Configuration of plugin with feed settings: Editor can configure feed by editing plugin content element
        * Correct length attribute for media file (https://github.com/georgringer/news/issues/916)
        * Channel image (TS constant plugin.tx_news.rss.channel.image)
        * Meta tag to notify search bots or feed readers
* EXT:image_autoresize to automatically resize down huge images on upload
* EXT:gridelements for grids
    * Provide Fluid template for gridelements content element (EXT:t3basic/Resources/Private/Templates/ContentElements/Gridelements.html)
    * Implement fully configurable slick slider (http://kenwheeler.github.io/slick/)
        * Include JS library only, if slider exists on page
    * Implement Bootstrap slider (https://getbootstrap.com/docs/3.3/javascript/#carousel)
* EXT:bootstrap_grids for predefined grids
    * Sorting of grid elements
    * Enable hiding on "extra small" devices
    * Remove settings for "large" and "extra large" devices

#2018-04-03  Sven Burkert  <bedienung@sbtheke.de>

* Version 6.0.0
* Update to TYPO3 8.7
* Development from scratch
* Optimize, enhance and bugfix EXT:bootstrap_package
    * Submit many bugfixes for EXT:bootstrap_package: https://github.com/benjaminkott/bootstrap_package/issues/created_by/SventB
    * Compare and optimize htaccess
    * Backend Layouts: Remove not needed layouts
    * Backend Layouts: Remove border and footer columns
    * Backend Layouts: Modified icons (without footer columns)
    * Backend Layouts: Slide for border columns
    * Optimize indexing of pages: exclude submenu (see https://github.com/benjaminkott/bootstrap_package/issues/461)
    * Optimize page templates: Add tag "main", remove attribute "role"
    * Optimize content templates
    * Optimized carousel with grid (2 columns)
    * Remove unneeded JavaScript files
    * Include latest jQuery library from Google CDN
    * Optimize images in columns (see https://github.com/benjaminkott/bootstrap_package/issues/465)
    * Remove classes "frame-space-before-none" and "frame-space-after-none"
    * Configuration of EXT:felogin, with Bootstrap template
    * Provide more content examples
    * Configuration of menus
    * Optimize and enhance (e.g. translation) RTE configuration
    * Improved and reasonable language configuration
    * Prevent default values for field "icon_color" and "icon_background" in table "tt_content"
    * Optimized headings: Prevent empty class attribute and not just one default layout, but 3.
    * Remove page type "popup" because it has errors, see https://github.com/benjaminkott/bootstrap_package/issues/476
    * Remove unnecessary crop variant "medium" for images
    * Support of preview images from PDF
* htaccess: Redirect to https
* Move favicon in root directory (for non-HTML pages like PDFs or images opened directly in browser)
* gzip for CSS and JS, disabled in development context
* Optimize typolink usage with option "addQueryString": Only for neccessary pages, else all parameters are accepted and EXT:realurl generates urls for them
* Disable RSA for login (BE and FE) when using HTTPS
* Optimize startpage (without redirect)
* Own theme (LESS/CSS) with possibility to exclude parts for better performance
* Lightbox for images
    * Transparent background in lightbox
    * Icon for images with lightbox
    * Show title and description in lightbox only once, if they are identical
* 404 error handling with detection of language
* Display 404 page for not existing language IDs
* Prepare 404 handling for multidomain
* Enable meta tags for page type "shortcut"
* Show records of type sys_file in list module and in search results in BE
* Add scheduler tasks for indexing files and file metadata
* Remove text "[Translate to xyz]" when translating content elements and records in context "Production"
* Configure preview domain for BE
* Add new backend layouts "Default 2 Columns 33/66" and "Default 2 Columns 66/33", mainly as example for new backend layouts
* Add new backend layout "Special Popup" for popups, lightbox content, ajax or other
* Show flag for content in default language
* Enable links between webroots in same TYPO3 backend
* Configuration of caching
    * Caching in frontend
    * Options for clear cache in BE for editors
    * Disable cache in development environment
* Backend user management according to http://typo3worx.eu/2017/02/typo3-backend-user-management/
* Optimize and fix print layout, e.g. make lazy loading of images compatible
* Option for header above content element in content element textpic and textmedia (otherwise the header is above the text)
* Search field in header
* Sticky header, optionally with transition
* Option for open all external links in new tab
* Reset default images for login logo and backend logo
* .typoscript as file ending for all TypoScript files
* Top and footer meta navigation
* TS constant to disable top meta navigation
* Add SVG icons to content element "uploads", as alternative to bootstrap glyphicons, configurable with TypoScript. Usage of free icons from https://www.iconfinder.com/iconsets/document-icons-2#
* Prepend file names of TypoScript files with a number (e.g. "1__setup.typoscript") to control order of inclusion
* Changes for field "frame_class"
    * Prevent that field "frame_class" of content element type "reference" gets value "default" by default
    * Enable multiple selection for field "frame_class"
    * Remove options "Default" and "None"
* Introduce new LESS constant "spacing-next-element" for spacing between content elements
* Introduce TS constant "extLinksBlank" to open external urls (from RTE and navigation) in a new tab
* RTE: Add second CSS file for custom styles
* Order of tabs in "New Content Element Wizard"
* Wrap contents in ext_localconf.php, ext_tables.php and Configuration/TCA/Overrides/ in a function because of performance and to ensure new variables are only available in current scope
* Ensure that TCA overrides (Configuration/TCA/Overrides/) only apply if extension is loaded
* Report and bugfix of BE search (see https://forge.typo3.org/issues/83538)
* Translate message "Page is being generated"
* Attribute "hreflang" in language menu
* Enable different browser title prefix for different languages
* Optimize Backend: Remove unneeded fields, set default values
* German translations for content elements from EXT:form and EXT:bootstrap_package
* Installation of EXT:unroll
* CSS: Automatic hyphenation for words
* EXT:crawler for indexing contents and records
* EXT:realurl
    * Implement EXT:realurl for speaking urls
    * Add scheduler tasks for cleaning up expired entries
    * Don't allow default language without language key (could cause duplicate content)
* EXT:cs_seo for search engine optimizations
    * Editor can set browser title directly
    * Preview of Google search result in page properties
    * Generation of robots.txt
    * Meta description and many other meta tags, e.g. og meta tags for social media (Facebook, Twitter)
    * hreflang link attribute in header
    * Canonical url (disabled by default)
    * Analysis of page, optional for a focus keyword
    * Support for third party extensions
    * Optional tracking with Google Analytics or Piwik
    * Disable tracking for Development context
    * XML sitemap for every language and for EXT:news
    * Remove SEO fields on pages where SEO fields are provided by plugin (e.g. on news detail page)
    * Enable EXT:cs_seo to generate browser title for EXT:news (see https://github.com/clickstorm/cs_seo/issues/162)
    * Set robots meta tag to "noindex,nofollow" for Development context
* EXT:gridelements for grids
    * New fluid content element
    * Implement fully configurable slick slider (http://kenwheeler.github.io/slick/)
        * Include JS library only, if slider exists on page
    * Implement Bootstrap slider (https://getbootstrap.com/docs/3.3/javascript/#carousel)
    * Grid with equal height of columns
    * Grid with minimum grid padding
* EXT:bootstrap_grids for grids and special content elements (tabs, accordion, slider) with Bootstrap Framework
* EXT:cewrap for wrapping of content elements
    * Enhance extension for usage in T3Basic
    * Content elements can be vertical centered (valign)
* EXT:cefooter for showing informations of a content element in the footer of the content element in TYPO3 BE in the page module
* EXT:news
    * Preconfiguration, optimized templates, styles, RSS feed, browser title, speaking url, breadcrumb path and language switch and meta data on detail page, lazy loading and adaptive images
    * Enable different layouts for LIST (2 columns, 3 columns, slider)
    * Enable different crop variants for images
    * Linkhandler to link on news detail pages with RTE
    * RSS feed
        * Feed can be enabled with TS constant
        * Configuration of plugin with feed settings
        * Correct length attribute for media file
        * Channel image (TS constant plugin.tx_news.rss.channel.image)
    * Optimize HTML structure for videos in news details
    * Valid HTML for tag "time" with attribute "itemprop=datePublished" (https://github.com/georgringer/news/issues/503)
    * New configuration option "newsFeed": Enable RSS feed, with meta tag to notify search bots or feed readers
    * Optimize template for category menu
    * Optimize pagination: Hide "prev" and "next" on mobile devices to enable more items on larger displays
    * Preview of news from BE with button "Save & Preview" (only for logged in BE users)
    * Enable BE search not only for uid and title, but more, like teaser and bodytext
    * Browser title contains "News:" (the page title is used as prefix)
    * alt attribute for dummy image
* EXT:min for minifying JS and CSS and cleaning up and minifying HTML source
    * Enable debug parameter together with pageNotFoundOnCHashError
* EXT:cookies for cookie usage hint, as cached version
* EXT:image_autoresize to automatically resize down huge images on upload
* EXT:form
    * Optimize Fluid templates
    * HTML output according to Bootstrap
    * Enable field for form field description
    * Store forms in /user_upload/forms/ (instead of /user_upload/)
    * Store uploaded files in forms in /user_upload/forms/uploaded_files/ (instead of /user_upload/)
    * Don't output form element "Content element" in email
    * Provide example contact form, with translation
* EXT:indexed_search for search
    * Configuration of EXT:crawler for automatic indexing
    * Enhancement of indexed_search ViewHelper class for better HTML (CSS classes, jQuery) and compatibility to Bootstrap
    * Nicer search icons with EXT:indexedsearch_icons
* EXT:shortcut_statuscodes: HTTP status code 301 for specific redirects (e.g. to startpage)
* EXT:default_upload_folder: Configure upload path of files which are uploaded via "Select & upload images", e.g. for content elements, pages or news

#2017-08-01  Sven Burkert  <bedienung@sbtheke.de>

* Version 5.6.1
* Update of extensions
* RTE: Correct text alignments
* Show records of type sys_file in list module and in search results in BE
* EXT:bootstrap_grids: Add "col-xs-12" to "simpleRow" grid element
* Prevent "display: inline" for lazy loading images in webkit browsers
* Ext:bootstrap_package and EXT:bootstrap_grids: Bugfix responsive images and image sizes in content element type "accordion" and "tabs"

#2017-02-17  Sven Burkert  <bedienung@sbtheke.de>

* Version 5.6.0
* Update to TYPO3 7.6.15
* Update of EXT:bootstrap_package
* Update of all other extensions
* Disable RSA for login when using HTTPS
* Improvements for language navigation / realurl link generation
* Optimize HTML structure for videos in news details
* Compare and optimize htaccess
* Set backend logo to default TYPO3 logo (not Bootstrap Package logo)
* Prevent default values for field "icon_color" and "icon_background" in table "tt_content"

#2016-10-07  Sven Burkert  <bedienung@sbtheke.de>

* Version 5.5.0
* Update to TYPO3 7.6.11
* Update of some extensions
* Update of EXT:news and compare of templates and configuration
* Update of EXT:bootstrap_package and compare of templates and configuration
* Use smooth scrolling of EXT:bootstrap_package
* Improvements on slider navigation arrows

#2016-07-08  Sven Burkert  <bedienung@sbtheke.de>

* Version 5.4.1
* Update of some extensions
* Update of htaccess
* New configuration option "newsFeed": Enable RSS feed for EXT:news
* Update configuration of error_reporting
* Check and handle errors in Deprecation and Sys Log

#2016-06-08  Sven Burkert  <bedienung@sbtheke.de>

* Version 5.4.0
* Permanent scroll to top link in footer
* Smooth scrolling

#2016-05-31  Sven Burkert  <bedienung@sbtheke.de>

* Version 5.3.0
* Update of some extensions
* Open all external links in new tab
* Remove "QuickEdit" in page module
* Improved 404 handling (show 404 error page with no redirect)
* Prepare 404 handling for multidomain
* Installation of EXT:rx_unrollsavebuttons
* Installation of EXT:sourceopt for cleaning up source code
* Improve template for content element "File Links"; enable more layout; prepare lightbox for preview image
* EXT:bootstrap_grids: Enable 1 column grid
* EXT:bootstrap_grids: Sorting of grids in new content element wizard
* Include jQuery with page.javascriptLibs
* Optimize slick slider configuration
* EXT:news: Optimize templates with grids
* EXT:news: Optimize paginate template
* EXT:news: Optimize category menu template
* Optimize CSS for content elements with background color

#2016-05-22  Sven Burkert  <bedienung@sbtheke.de>

* Version 5.2.1
* Changes in content element "File Links" (open download dialog instead of opening file in browser)

#2016-05-03  Sven Burkert  <bedienung@sbtheke.de>

* Version 5.2.0
* Add option to disable lazy loading/adaptive images
* Improve lazy loading images in sliders
* Improved configuration for slick slider
* Extended configuration for slick slider (carousel)

#2016-03-17  Sven Burkert  <bedienung@sbtheke.de>

* Version 5.1.8
* Optimize print layout

#2016-03-16  Sven Burkert  <bedienung@sbtheke.de>

* Version 5.1.7
* Optimize meta tags on news detail page
* EXT:news: Image for RSS feed

#2016-03-09  Sven Burkert  <bedienung@sbtheke.de>

* Version 5.1.6
* Responsive tables (with scrollbar on smaller screen sizes)

#2016-03-08  Sven Burkert  <bedienung@sbtheke.de>

* Version 5.1.5
* Bugfix lists (ul-tag), add arrow control element before li-tag

#2016-03-07  Sven Burkert  <bedienung@sbtheke.de>

* Version 5.1.4
* EXT:news: RSS feed bugfix and TS improvement

#2016-02-29  Sven Burkert  <bedienung@sbtheke.de>

* Version 5.1.3
* Enable images in RTE, with clickenlarge
* Improve configuration of slick slider

#2016-02-24  Sven Burkert  <bedienung@sbtheke.de>

* Version 5.1.2
* Optimize layout template of frames (section_frame)

#2016-02-23  Sven Burkert  <bedienung@sbtheke.de>

* Version 5.1.1
* RTE: Add example for custom classes of links

#2016-02-18  Sven Burkert  <bedienung@sbtheke.de>

* Version 5.1.0
* EXT:news: Enable different layouts for LIST (2 columns, 3 columns and slider)

#2016-02-17  Sven Burkert  <bedienung@sbtheke.de>

* Version 5.0.0
* Repeal EXT:t3basic_mypage, all configurations are done in EXT:t3basic now. Reasons for that: Improve performance (e.g. for fluid template paths), easier and more clear configuration (e.g. for fluid template paths, especially for content elements), no need to rename EXT:t3basic_mypage.

#2016-02-16  Sven Burkert  <bedienung@sbtheke.de>

* Version 4.6.7
* New configuration option "linkWizardBlindLinkFields": Remove attribute fields in link wizards.
* New configuration option "linkCaption": If true, the caption of an image is also linked if an image is linked.
* Replace animated loading GIF with SVG

#2016-02-15  Sven Burkert  <bedienung@sbtheke.de>

* Version 4.6.6
* Update of some extensions
* German translations in content element wizard
* Bugfix templates of EXT:form
* Configurable start level of breadcrumb menu

#2016-02-13  Sven Burkert  <bedienung@sbtheke.de>

* Version 4.6.5
* Add SVG icons to content element "uploads", as alternative to bootstrap glyphicons, configurable with TypoScript. Usage of free icons from here: https://www.iconfinder.com/iconsets/document-icons-2
* Optimize CSS: Margin of headers to previous content element
* Make dropdown of main menu configurable. If disabled, menu for small devices contain dropdown.

#2016-02-11  Sven Burkert  <bedienung@sbtheke.de>

* Version 4.6.4
* Set size of field "header_position" of table "tt_content" from varchar(6) to varchar(100)

#2016-02-10  Sven Burkert  <bedienung@sbtheke.de>

* Version 4.6.3
* Enable external URL and mailto links in navigations

#2016-02-08  Sven Burkert  <bedienung@sbtheke.de>

* Version 4.6.2
* Optimize JS includes
* Translate labels of "section_frame"
* Introduce new frames "Box with background" and "Minimum Grid"
* Optimize Bootstrap CSS includes: Do not include everything
* Optimize LESS files of theme: Offer multiple files instead of one big file

#2016-01-27  Sven Burkert  <bedienung@sbtheke.de>

* Version 4.6.1
* Improve Fluid content template for menu type "section index"

#2016-01-20  Sven Burkert  <bedienung@sbtheke.de>

* Version 4.6.0
* Configure preview domain for BE
* Update of EXT:bootstrap_package
* Implement EXT:linkhandler for links to detail pages of records
* Configure EXT:linkhandler for EXT:news

#2016-01-19  Sven Burkert  <bedienung@sbtheke.de>

* Version 4.5.0
* Add mobile navigation for navigation levels > 2
* Add new option "margin" for slick slider
* Optimize content elements type "Shortcut": No wrapping with div-tag
* Add new grid examples and enable equal height grids

#2016-01-18  Sven Burkert  <bedienung@sbtheke.de>

* Version 4.4.2
* EXT:news RSS bugfix
* Add new backend layouts 33/66 and 66/33, mainly as example for new backend layouts
* Change configuration of start page

#2016-01-15  Sven Burkert  <bedienung@sbtheke.de>

* Version 4.4.1
* Provide more content examples
* Optimize RTE configuration and tables
* Optimize configuration for editor user

#2016-01-13  Sven Burkert  <bedienung@sbtheke.de>

* Version 4.4.0
* Implement lightbox with arrows, navigation, keyboard and touch control, image preload
* Implement fully configurable slider with EXT:gridelements and slick

#2016-01-12  Sven Burkert  <bedienung@sbtheke.de>

* Version 4.3.1
* Enable content elements below navigation
* Enable "linkToTop" feature
* Clean up labels and descriptions in backend

#2016-01-11  Sven Burkert  <bedienung@sbtheke.de>

* Version 4.3.0
* New Fluid content element for EXT:gridelements
* Bugfix Fluid content element "felogin"
* Setting for headers above or besides image in content element textpic and textmedia
* Preconfigure different page layouts

#2016-01-10  Sven Burkert  <bedienung@sbtheke.de>

* Version 4.2.0
* Update to TYPO3 7.6.2
* Update of extensions: crawler, gridelements, seo_basics
* Update of EXT:bootstrap_package: Fluid content elements and menus
* Adaptions of main menu, sub menu, breadcrumb menu, language menu
* Top and footer menu with Fluid template
* Hint: Image in text (floating) not possible any more because of Fluid content elements

#2016-01-03  Sven Burkert  <bedienung@sbtheke.de>

* EXT:bootstrap_grids: Add class "col-xs-12" to grid element "Row"

#2015-12-14  Sven Burkert  <bedienung@sbtheke.de>

* Implement slider with content element "Images Only" and slick

#2015-12-08  Sven Burkert  <bedienung@sbtheke.de>

* Version 4.1.0
* Reintegrate EXT:gridelements
* EXT:bootstrap_grids for grids and special content elements (tabs, accordion, slider) with Bootstrap Framework
    * Bugfix for EXT:bootstrap_grids (see https://github.com/laxap/bootstrap_grids/issues/2)
* Implement slider with EXT:gridelements and Bootstrap Framework
* Implement slider with EXT:gridelements and slick

#2015-12-01  Sven Burkert  <bedienung@sbtheke.de>

* Version 4.0.0
* Update to TYPO3 7.6 (new, clean installation)
* Update to EXT:realurl version 2.x (see https://github.com/dmitryd/typo3-realurl)
* Remove deprecated typo3conf/extTables.php
* 404 error handling with detection of language
* Improve layout and configuration of menu (desktop and mobile)
* Use bttrlazyloading for lazy loading and adaptive images
* EXT:news with preconfiguration, optimized templates, styles, RSS feed, browser title, breadcrumb path and meta data on detail page, lazy loading and adaptive images
* New configuration option for removing "Translate to" message when translating content elements and records
* Support for disabled JavaScript: changes to menu and images
* Optimize indexed contents (TYPO3SEARCH_begin)
* Scheduler job for truncating indexing tables
* EXT:crawler for indexing contents and records
* Optimize Fluid templates and email templates for EXT:form

#2015-07-07  Sven Burkert  <bedienung@sbtheke.de>

* Version 3.0.1
* Update of TYPO3 system and extensions
* Modify backend layout icons
* Optimize breadcrumb menu
* Configuration of contact form: Set redirect page
* Allow JS and CSS files in robots.txt
* Change searchbox in header from USER_INT to USER

#2014-09-25  Sven Burkert  <bedienung@sbtheke.de>

* Version 3.0.0
* New setup with TYPO3 version 6.2
* Usage and enhancement of EXT:bootstrap_package
    * Copyright with current year (https://github.com/benjaminkott/bootstrap_package/issues/72)
    * Meta tag robots=noindex (https://github.com/benjaminkott/bootstrap_package/issues/69)
    * Skip to content (https://github.com/benjaminkott/bootstrap_package/issues/63)
    * Improve TypoScript configuration (https://github.com/benjaminkott/bootstrap_package/issues/57)
    * Google Analytics tracking code anonymization (https://github.com/benjaminkott/bootstrap_package/issues/84)
    * Improve RTE configuration (https://github.com/benjaminkott/bootstrap_package/issues/144)
* Usage of Bootstrap Framework
* Further configuration and improvements with EXT:t3basic
    * Remove footer content
    * Improved TYPO3 system configuration
    * Improved RTE configuration, preconfiguration of individual styles and different special tags
    * Improved realurl configuration
    * Improved meta tags
    * Configuration of EXT:felogin, with Bootstrap template
    * Configuration of EXT:indexed_search, nicer search icons with EXT:indexedsearch_icons
    * Configuration of second webpage in same TYPO3 BE, enable linking between them
    * Line breaks for headings
    * Automatic headers: First header is H1, all other h2, headers not in main columns are h3
    * Top and bottom meta navigation (https://github.com/benjaminkott/bootstrap_package/issues/75)
    * No baseURL/base href
    * Content sliding for right and left columns
    * Print layout
    * Improvements to text/image rendering (tt_content)
    * Enable meta tags for page type "shortcut"
    * Much more
* EXT:t3basic_mypage for individual configuration, templates and design (modify this extension as you wish)
* Display access restricted contents for BE users
* Add robots.txt
* Replace login and BE TYPO3 logo
* Redirect to www
* More content examples
* Spam check for forms with EXT:wt_spamshield
* Search engine optimization with EXT:seo_basics (XML sitemap, individual browser title, canonical url)
* Enhance editor group
* Monitoring of TYPO3 system and extensions with EXT:caretaker_instance
* Wrapping of content elements with EXT:cewrap

#2014-01-01  Sven Burkert  <bedienung@sbtheke.de>

* Version 2.7.1
* Remove config.baseURL and t3basic.config.domain
* Fix editor access rights for EXT:gridelements
* Translation of EXT:powermail now fixed
* Include latest jquery and jqueryui libraries from google libraries
* EXT:powermail: Disable HTML5 form validation
* Update to TYPO3 version 4.7.17
* Update of extensions

#2013-12-05  Sven Burkert  <bedienung@sbtheke.de>

* Version 2.7.0
* Accordion with gridelements
* Add default mail settings in localconf.php (email and name)
* Linking of image captions if image has a link
* Optimize realurl (cHash problem)
* Use GraphicsMagick instead of ImageMagick
* EXT:gridelements: grid with 5 cols
* Better integration of print css
* Optimize browser title for standard pages and for news detail pages
* Optimize support of IE (body class, without TS conditions)
* Update to TYPO3 version 4.7.16
* Update to jQuery version 1.10.2
* Update of extensions

#2013-07-21  Sven Burkert  <bedienung@sbtheke.de>

* Version 2.6.0
* Slider with gridelements
* Change configuration of startpage
* Fix redirect problem with sitemap

#2013-07-10  Sven Burkert  <bedienung@sbtheke.de>

* Version 2.5.0
* Change flags for language navigation
* Changes to form CSS, better support of IE10
* Set expire header for images
* Optimize menu (TS and CSS)
* Update to TYPO3 version 4.7.12
* Update of extensions

#2013-05-17  Sven Burkert  <bedienung@sbtheke.de>

* Version 2.4.8
* Update to TYPO3 version 4.7.11
* RTE configuration: remove deprecated settings
* Restructure of folder t3basic in fileadmin and TypoScript configuration
* Update of extensions

#2013-04-22  Sven Burkert  <bedienung@sbtheke.de>

* Version 2.4.7
* Update to TYPO3 version 4.7.10
* Remove br-tag for captions
* Update of extensions

#2013-03-12  Sven Burkert  <bedienung@sbtheke.de>

* Version 2.4.5
* Update of EXT:gridelements

#2013-02-18  Sven Burkert  <bedienung@sbtheke.de>

* Version 2.4.4
* Update to jQuery version 1.9.1
* Update to TYPO3 version 4.7.8

#2013-01-21  Sven Burkert  <bedienung@sbtheke.de>

* Version 2.4.3
* Update of extensions
* Configuration of EXT:felogin

#2012-12-03  Sven Burkert  <bedienung@sbtheke.de>

* Version 2.4.2
* New template "popup" for popups, lightbox content and ajax
* Changes in navigation.css; preconfigure vertical list

#2012-11-20  Sven Burkert  <bedienung@sbtheke.de>

* Version 2.4.1
* Update to TYPO3 version 4.7.7
* Bugfix of EXT:seo_basics (see http://forge.typo3.org/issues/37606)
* Enhance EXT:basics for TYPO3 version 4.7
* Improve responsive design for images

#2012-11-07  Sven Burkert  <bedienung@sbtheke.de>

* Version 2.4.0
* Language menu now is able to handle records (like tt_news)

#2012-10-09  Sven Burkert  <bedienung@sbtheke.de>

* Version 2.3.0
* Responsive design
* Rearrange TypoScript configuration
* Change RTE configuration
* Update to TYPO3 version 4.7.4, update of extensions
* Remove EXT:rlmp_flashdetection

#2012-08-06  Sven Burkert  <bedienung@sbtheke.de>

* Version 2.2.2
* Improve grid elements

#2012-07-25  Sven Burkert  <bedienung@sbtheke.de>

* Version 2.2.1
* Update to TYPO3 version 4.7.2
* Prepare indexed_search for multilanguage site

#2012-06-14  Sven Burkert  <bedienung@sbtheke.de>

* Version 2.2.0
* Update of EXT:powermail to version 2.0.0 (with Extbase/Fluid), optimizations to form rendering
* Adaption of editor (BE user)
* Implement EXT:imagewidthspecificationwizard

#2012-05-14  Sven Burkert  <bedienung@sbtheke.de>

* Version 2.1.1
* Changes in Lightbox skin

#2012-04-25  Sven Burkert  <bedienung@sbtheke.de>

* Version 2.1.0
* Update to TYPO3 version 4.7
* HTML5 video and audio
* Improve HTML5 source code
* Config for DEV system (localconf.php)

#2012-03-16  Sven Burkert  <bedienung@sbtheke.de>

* Version 2.0.0
* HTML5
* new YAML version and reorganize structure
* Update to TYPO3 version 4.6
* Remove EXT:templavoila, use EXT:gridelements
* reorganize TypoScript structure

#2012-01-30  Sven Burkert  <bedienung@sbtheke.de>

* Version 1.0.0
* Initial release (development since 2006)
