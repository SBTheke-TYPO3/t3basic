<?php
defined('TYPO3_MODE') or die();

(function($extKey) { // Wrap code in function [TASK-be-2]

    // Add Page TSconfig [TASK-be-1]
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:t3basic/Configuration/TsConfig/page.tsconfig">');

    // Multidomain Configuration [TASK-config-2]
    #$domain = ltrim(\TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('HTTP_HOST'), 'www.');
    #OPTION 1: if(stripos($domain, 'root2.') !== false) {}
    #OPTION 2: if(in_array($domain, array('root2.basic95.local', 'root2.basic.de'))) {}

    // Overwrite language keys of TYPO3 [TASK-ui-4]
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf'][] = 'EXT:' . $extKey . '/Resources/Private/Language/Overrides/locallang_db_new_content_el.xlf';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['EXT:backend/Resources/Private/Language/locallang_alt_doc.xlf'][] = 'EXT:' . $extKey . '/Resources/Private/Language/Overrides/locallang_alt_doc.xlf';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['EXT:frontend/Resources/Private/Language/locallang_ttc.xlf'][] = 'EXT:' . $extKey . '/Resources/Private/Language/Overrides/locallang_ttc.xlf';
    // Overwrite translation of EXT:bootstrap_package (mainly german translations) [TASK-language-2]
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['EXT:bootstrap_package/Resources/Private/Language/locallang.xlf'][] = 'EXT:' . $extKey . '/Resources/Private/Language/Overrides/Extension/bootstrap_package.frontend.xlf';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['EXT:bootstrap_package/Resources/Private/Language/Backend.xlf'][] = 'EXT:' . $extKey . '/Resources/Private/Language/Overrides/Extension/bootstrap_package.backend.xlf';
    // Overwrite translation of EXT:bootstrap_grids [TASK-bootstrapgrids-2]
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['EXT:bootstrap_grids/Resources/Private/Language/locallang_db.xlf'][] = 'EXT:' . $extKey . '/Resources/Private/Language/Overrides/Extension/bootstrap_grids.backend.xlf';

    // EXT:rte_ckeditor: Configuration [TASK-rte-1]
    $GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['bootstrap'] = 'EXT:t3basic/Configuration/Yaml/Extension/RteCkeditor/Default.yaml';
    $GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['default']   = 'EXT:t3basic/Configuration/Yaml/Extension/RteCkeditor/Default.yaml';

    // EXT:cs_seo: Analysis of records of EXT:news [TASK-seo-5]
    $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['cs_seo']['yamlConfigFile'] = 'EXT:t3basic/Configuration/Yaml/Extension/CsSeo/Records.yaml';

})('t3basic');
