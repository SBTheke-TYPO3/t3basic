=============
Documentation
=============

----------------
What does it do?
----------------

- Configuration and enhancements of TYPO3
- Enhancement of EXT:bootstrap_package
- Many more (see CHANGELOG.md)


-----
Hints
-----

Placeholders
============

Use placeholders:

- ###FRONTEND_URL###: Frontend url
- ###CURRENTYEAR###: Current year
- ###SPACE###: Simple whitespace


TS constants
============

You can access TypoScript constants in Fluid (because of BK2K\BootstrapPackage\DataProcessing\ConstantsProcessor)


Tooltips
========

For tooltips, Popper.js is implemented (https://popper.js.org/)


Share images
============

To enable this feature for the lightbox: copy, modify (option "shareEl") and use JavaScript file EXT:bootstrap_package/Resources/Public/JavaScript/Src/bootstrap.lightbox.js


SCSS
====

Use variables as often as possible:

- $spacer: Margin (margin-bottom preferred) to next element (in rem)
- $grid-gutter-width: Default width (in px)
- more: tbd


------
Errors
------

- No space between buttons: use list (ul) with style "list-inline" or remove f:spaceless


----------
To-Do list
----------

You have ideas? Contact me!


---------
ChangeLog
---------

See file **CHANGELOG.md** in the extension directory.
