/**
 * Implement fully configurable slick slider [FEATURE-slider-1]
 */
$(function() {

    var slickDefault = {
        arrows: false,
        dots: false,
        easing: 'swing',
        prevArrow: '<a class="left carousel-control carousel-control-prev" role="button"><span class="carousel-control-icon carousel-control-prev-icon"></span><span class="sr-only">Previous</span></a>',
        nextArrow: '<a class="right carousel-control carousel-control-next" role="button"><span class="carousel-control-icon carousel-control-next-icon"></span><span class="sr-only">Next</span></a>',
        dotsClass: 'carousel-indicators',
        responsive: [{
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    };

    // Slider for gridelements
    // $('.slick-gridelements').slick();
    // @todo: Use newest version of slick and remove following code, when this bug is fixed: https://github.com/kenwheeler/slick/issues/2711
    $('.slick-gridelements').each(function() {
        var slickOptions = jQuery.extend(true, {}, slickDefault);
        var options = $(this).attr('data-slick');
        if(options) {
            $.extend(slickOptions, $.parseJSON('{' + options + '}'));
        }
        $(this).slick(slickOptions);
    });

    // Slider for content element "Images only"
    var slickDefaultImages = jQuery.extend(true, {}, slickDefault);
    $.extend(slickDefaultImages, {
        arrows: true,
        dots: true,
        infinite: true,
        speed: 300,
        adaptiveHeight: true,
    });

    $('.slick-images').each(function() {
        var slickOptions = jQuery.extend(true, {}, slickDefaultImages);
        var options = $(this).closest('.slick-images').attr('data-slider');
        if(options) {
            $.extend(slickOptions, $.parseJSON('{' + options + '}'));
        }
        $(this).slick(slickOptions);
    });

    // Slider for news
    var slickOptions = jQuery.extend(true, {}, slickDefault);
    $.extend(slickOptions, {
        arrows: true,
        dots: true,
        autoplay: true,
        autoplaySpeed: 10000,
    });
    $('.news-list-view-layout-slider').slick(slickOptions);

    // Remove unwanted lightbox images
    $('.carousel').on('init', function(event, slick) {
        slick.$slider.find('.slick-cloned').find('a.lightbox').removeClass('lightbox');
    });
});
