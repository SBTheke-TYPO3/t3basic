/**
 * Implement Bootstrap slider [FEATURE-slider-2]
 */
$(function() {
    $('.carousel').each(function () {
        $(this).carouselHeights();
    });
});

// Normalize Bootstrap Carousel Heights
// @see https://coderwall.com/p/uf2pka/normalize-twitter-bootstrap-carousel-slide-heights
$.fn.carouselHeights = function() {
    var items = $(this).find('.item'), // grab all slides
        heights = [], // create empty array to store height values
        tallest, // create variable to make note of the tallest slide
        call;
    var normalizeHeights = function() {
        items.each(function() { // add heights to array
            heights.push($(this).outerHeight());
        });
        tallest = Math.max.apply(null, heights); // cache largest value
        items.css('height', tallest);
    };
    normalizeHeights();
    $(window).on('resize orientationchange', function() {
        // reset vars
        tallest = 0;
        heights.length = 0;
        items.css('height', ''); // reset height
        if(call){
            clearTimeout(call);
        };
        call = setTimeout(normalizeHeights, 100); // run it again
    });
};
