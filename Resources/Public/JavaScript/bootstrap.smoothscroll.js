(function($){

    //
    // Smooth Scroll
    // Improved smooth scroll [TASK-javascript-3]
    // - top padding
    // - correct top padding when opening url with anchor directly
    // - provide "#scrollHere" for scrolling to target element when opening a page
    //
    $('a[href*="#"]:not([href$="#"])').click(function(e) {
        if (location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'')
            && location.hostname === this.hostname
            && $(this).data('toggle') === undefined
            && $(this).data('slide') === undefined) {
            var $target = $(this.hash.replace( /(:|\.|\[|\]|,|=|\/)/g, '\\$1'));
            $target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');
            smoothScroll($target);
            e.preventDefault();
        }
    });

    if(window.location.hash) {
        try {
            smoothScroll($(window.location.hash));
        } catch (e) {
            // Catch problems with invalid hash (e.g. with "=" or "&" in it)
        }
    }

    if($('#scrollHere').length) {
        smoothScroll($('#scrollHere'));
    }

    //
    // Scroll to top
    //
    $('.scroll-top').on('click', function() {
        $(this).blur();
    });
    $(window).on('scroll', function () {
        if ($(this).scrollTop() > 300) {
            $('.scroll-top').addClass('scroll-top-visible');
        } else {
            $('.scroll-top').removeClass('scroll-top-visible');
        }
    });

})(jQuery);

function smoothScroll(target) {
    if (target.length) {
        var offset = 20;
        var targetOffset = target.offset().top - offset;
        var navbar = $('.navbar-fixed-top');
        var navbarHeight = 0;
        if(navbar.length && targetOffset !== 0) {
            navbarHeight = navbar.outerHeight();
        }
        $('html,body').animate({scrollTop: targetOffset - navbarHeight}, 500, 'swing', function() {
            // Check navbar height again (it could have a transition)
            if(navbarHeight && navbarHeight != navbar.outerHeight()) {
                $('html,body').animate({scrollTop: targetOffset - navbar.outerHeight()}, 500);
            }
        });
    }
}
